#!/usr/bin/env bash
#
# Script name: build-db.sh
# Description: Script for rebuilding the database for dtos-core-repo.
# GitLab: https://www.gitlab.com/dwt1/dtos-core-repo
# Contributors: Derek Taylor

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

echo "###########################"
echo "Building the repo database."
echo "###########################"

## Arch: x86_64
cd x86_64
rm -f sedspkgs*

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

## repo-add
## -n: only add new packages not already in database
## -R: remove old package files when updating their entry
repo-add -R -n sedspkgs.db.tar.gz *.pkg.tar.zst

# Removing the symlinks because GitLab can't handle them.
rm sedspkgs.db
rm sedspkgs.files

# Renaming the tar.gz files without the extension.
mv sedspkgs.db.tar.gz sedspkgs.db
mv sedspkgs.files.tar.gz sedspkgs.files

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"
